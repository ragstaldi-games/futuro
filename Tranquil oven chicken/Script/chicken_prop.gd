extends Sprite2D

var perfect = load("res://Assets/Perfect_Chicken.png")
var burn = load("res://Assets/Burnt_chicken.png")
var raw = load("res://Assets/Raw_chicken.png")
var alien = load("res://Assets/Space_chicken.png")

# Called when the node enters the scene tree for the first time.
func _ready():
	if Global.chicken_state == Global.chicken.PERFECT:
		texture = perfect
	elif Global.chicken_state == Global.chicken.BURNT:
		texture = burn
	elif Global.chicken_state == Global.chicken.RAW:
		texture = raw
	elif Global.chicken_state == Global.chicken.ALIEN:
		texture = alien			


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func _on_button_pressed():
	get_tree().root.get_node("Game manager").change_node("end")
