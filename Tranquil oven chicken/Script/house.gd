extends Control

@onready var animator : AnimationPlayer = $AnimationPlayer

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func _on_button_pressed():
	animator.play("Open_door")
	$AudioStreamPlayer2D.play()
	$Button.hide()


func _on_animation_player_animation_finished(anim_name):
	get_parent().change_node("bell")
