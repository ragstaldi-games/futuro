extends Control

var scene_name = ""

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func fade_in(name):
	show()
	$AnimationPlayer.play("fade_in")
	scene_name = name

func fade_out():
	$AnimationPlayer.play("fade_out")
	hide()	


func _on_animation_player_animation_finished(anim_name):
	if anim_name == "fade_in":
		get_tree().root.get_node("Game manager").change_node(scene_name)
		
	if anim_name == fade_out()	
