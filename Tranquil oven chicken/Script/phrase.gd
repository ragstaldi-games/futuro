extends Control

@onready var phrase : Label = $Label
@onready var author : Label = $Label2
@onready var animator : AnimationPlayer = $AnimationPlayer

# Called when the node enters the scene tree for the first time.
func _ready():
	phrase.visible_ratio = 0
	author.visible_ratio = 0
	animator.play("write_phrase")
	$AudioStreamPlayer2D.play()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func _on_animation_player_animation_finished(anim_name):
	if anim_name == "write_phrase":
		animator.play("write_author")
		$AudioStreamPlayer2D.play()
		
	if anim_name == "write_author":
		get_parent().change_node("mountains")	
