extends Control

var stage = 0
var option = 1

var situations = ["Un amigo te cuenta un secreto ¿Qué haces?", "Le mentiste a tu pareja ¿Qué haces?", "Tenes una gran oportunidad de trabajo pero tenes que dejar el país ¿Qué haces?", "¿Dejarias todo por el sueño indie?"]



var options1_1 = ["Se lo cuento a todo el mundo.", "Dejo que la mentira crezca.", "La tomo.", "No, me da miedo."]
var options2_1 = ["Se lo cuento solo a familiares.", "Me victimizo.", "Me quedo en el país.", "Solo si el exito esta asegurado."]
var options3_1 = ["No se lo cuento a nadie, es un secreto.", "Le digo la verdad y me disculpo.", "Evaluo las condiciones del trabajo, y del pais destino.", "Si."]
var options4_1 = ["Creo un videojuego sobre ese secreto.", "Me cambio de provincia e identidad.", "Los denuncio por explotacion laboral", "Hago juegos de ***** con anuncios"]

var answer1_1 = ["Tu amigo se entera y te confronta", "Tu pareja descubre la verdad y se enoja", "Te vas del país, pero es dificil acostumbrarte a tu nueva vida", "Tus amigos y familiares insisten, porque tenes el talento, pero te quedas en el molde"]

var options1_2 = ["No te tomas la situacion enserio y se pelean", "La insultas y te vas", "Te juntas con gente indebida, y dejas de trabajar", "No haces nada"]
var options2_2 = ["No le decis nada y te vas silenciosamente", "Decis: '¿Qué queres que haga?'","Seguís trabajando sin gestionar tus emociones", "Tenes ideas, pero no prototipas"]
var options3_2 = ["Le sos sincero, y te disculpas por tus actos", "Hablas con ella", "Te das cuenta que no es para vos, y volves a tu país", "Haces un juego y lo mandas a EVA"]
var options4_2 = ["Le regalas un auto y que no joda mas", "Te casas con una supermodelo", "Te volves el capo de una mafia albanesa", "Pones un parripollo en la estacion de Lanús"]

var answer1_2 = ["Tu amigo te dice que sos un traidor, y que no le hables nunca mas.", "Entre lagrimas te dice que te queria, y te desea lo mejor", "Te atrapa la policia", "El tren paso, ahora tenes que trabajar"]
var answer1_3 = ["Tu amigo te insulta mientras te vas", "Te pega una cachetada y se va", "Te quebras y no podes mas", "Ningun publisher te acepta tu idea"]
var answer1_4 = ["Tu amigo acepta tu disculpa, pero sigue enojado", "Solucionan las cosas", "Llegas a casa feliz, listo para perseguir tu sueño", "El juego queda, y a la gente le encanta"]
var answer1_5 = ["Tu amigo toma el auto desconcertado, pero enojado tambien", "Se van a vivr a las Bahamas, pero tu novia queda triste", "Tenes que cerrar un trato millonario con un mafioso italiano", "Vendes altos pollos"]

var answer2_1 = ["Tu amigo no se entera, pero la culpa te carcome", "Tienen una fuerte pelea, y se separan", "Te arrepentis de no haber tomado la decision, no podes sentar cabeza", "Tardas en arrancar, y el camino se vuelve cada vez mas cuesta arriba"]

var options1_3 = ["Se lo contas a un amigo bocon", "La defenestras por redes", "Recurris a la delincuencia", "Utilizas practicas poco eticas"]
var options2_3 = ["Te volves paranoico por no hablar", "Recurris al alcohol para ahogar tus penas", "Te deprimis", "Terminas abandonando tu sueño"]
var options3_3 = ["Te juntas con tu amigo y le contas la verdad", "Hablas con ella luego de un tiempo, disculpandote", "Te levantas y buscas nuevas oportunidades", "Seguis adelante"]
var options4_3 = ["Abris un cabaret", "Te vas a Las Vegas", "Creas un programa de afiliados", "Te vas a Marte"]

var answer2_2 = ["Ese amigo se lo cuenta a tu amigo, y te confronta", "Tu pareja entra en un cuadro depresivo y termina internada", "Despues de muchos robos, le robas a una anciana en la calle, que te pide clemencia", "Tus juegos se vuelven exitosos a costa de estafas"]
var answer2_3 = ["La paranoia se vuelve reclusion social, y no salis de tu casa", "El alcohlismo te lleva a manejar borracho, cuando derrepente ves una persona por la calle", "La vida no tiene color, y los antidepresivos no ayudan", "Tenes un trabajo y una vida que odias"]
var answer2_4 = ["Tu amigo se enoja, pero acepta tus disculpas y dice que necesita un tiempo para reflexionar", "Agradece que te hayas disculpado, y te cuenta que tiene una pareja nueva", "Las nuevas oportunidades llegan gracias a tu esfuerzo", "Logras sacar juegos interesantes, y conseguis audiencia"]
var answer2_5 = ["Te llenas de plata con el cabaret, y quieren mas locales", "Ganas la apuesta de todo al verde, y dejas en bancarrota un casino", "Estafas a mucha gente, y te llaman a juicio", "Pones una granja de papas en Marte"]


var answer3_1 = ["Tu amigo confia en vos, y se lo demostras.", "Se enoja porque le mentiste, pero lo charlan y lo solucionan.", "El trabajo parece prometedor, pero hay elementos que no te convencen. Crees en tu juicio, y vas a elegir lo mejor", "Empezas con mucho entusiasmo, y creas muchos juegos. Aqui comienza tu historia"]

var options1_4 = ["Le tenes envidia y le robas la novia", "La engañas con su hermana", "Rechazas el trabajo", "Arruinas tu prestigio por actos irresponsables"]
var options2_4 = ["Te enteras que lo engañan, pero no le contas", "No resolves problemas como prometiste", "Terminas no eligiendo y te conformas", "Perdes el entusiasmo y abandonas"]
var options3_4 = ["Le contas un secreto a tu amigo", "Le propones casamiento", "Te quedas en tu hogar para cumplir tu sueño", "Creas los juegos con los que soñas"]
var options4_4 = ["Se vuelven presidente y vicepresidente del pais", "Crean un imperio global y nuevo orden mundial","Te volves piloto de avion", "Registras la industria de videojuegos como tu propiedad" ]

var answer3_2 = ["Tu amigo, traicionado, se enfrenta a vos con mucha rabia", "Tu novia no lo puede creer, y entre lagrimas te dice que te vayas", "No pensaste otras opciones, y te volves un vago", "La gente pide que se te meta preso por estafador y degenerado"]



var answer3_3 = ["Tu amigo se entera de que lo engañan, y te pregunta si sabias", "Tu novia se vuelve a enojar y te da un ultimatum", "Tenes trabajo estable, pero tenes la espina de querer mas", "Sentis que tu desicion fue erronea"]


var answer3_4 = ["Tu amigo confia en vos, y guarda el secreto como una tumba", "Dice que sí", "Trabajas duro por tu sueño, y lo cumplis. Nace Ragstaldi Games", "Todos esos juegos con los que soñas desde chico salen a la luz"]


var answer3_5 = ["El pais tiene una gran prosperidad, pero mucha corrupcion", "Dominan al mundo y eliminan el libre pensamiento", "Podes volar de Buenos Aires a Japon en 3 horas", "Todo aquel que haga un videojuego debe pagarte, los desarrolladores no estan contentos"]


var answer4_1 = ["El videojuego se vuelve hiper popular y te volves multimillonario", "Empezas tu nueva vida como pescador en Tierra del fuego", "Ganas el juicio y te volves CEO de la empresa", "Te contratan como game designer de From Software"]

var options1_5 = ["Te gastas la plata en apuestas", "Matas una persona por envidia", "Llevas la empresa a la quiebra", "Creas juegos macabros y censuran el estudio"]
var options2_5 = ["No sabes que hacer con tanta plata", "Continuas pescando por el resto de tus dias", "Terminas perdiendo el puesto por inoperancia", "No sacas ningun juego por indesicion"]
var options3_5 = ["Fue deshonesto, y le das todas las ganancias a tu amigo, con una disculpa", "Volves y amendas tus errores", "Cambias las politicas de la empresa para un ambiente sano", "Creas grandes juegos"]
var options4_5 = ["Compras una mansion que orbita en Jupiter", "Te abducen unos aliens", "Vendes la empresa y abris una parrilla en Chubut", "Revivis a Elvis Presley"]

var answer4_2 = ["No te queda un peso partido al medio, y tu amigo aparece con una denuncia por difamacion", "Vas a juicio por tus actos", "Los inversores y empleados quieren tu cabeza", "Entras en una batalla legal con los representantes del estudio"]


var answer4_3 = ["Terminas perdiendo mucha plata en juicios contra tu amigo", "Tenes una vida tranquila, pero el remordimiento siempre esta", "No podes conseguir otro trabajo, y terminas en la calle", "Te despiden"]


var answer4_4 = ["Tu amigo acepta ambas cosas, y buscas limpiar tu nombre", "Tu novia esta feliz de verte, y mejoran su situacion", "Volves a la empresa la mejor del mundo, y te retiras en gloria", "Tus juegos son exitosos, y se vuelven leyendas de la industria"]


var answer4_5 = ["La mansion esta espectacular, y tiene todos los chiches que te imaginas", "Los aliens iban a experimentar con vos, pero te volves su lider", "La parrilla es un exito, y vienen comensales de todo el mundo", "Junto a Elvis, crean la saga Presley Souls, sacando 32 entregas de este"]




var random_number



# Called when the node enters the scene tree for the first time.
func _ready():
	random_number = randi() % 4
	$Situation.text = situations[random_number]
	$cc1/Option1/txt1.text = options1_1[random_number]
	$cc1/Option2/txt2.text = options2_1[random_number]
	$cc1/Option3/txt3.text = options3_1[random_number]
	$cc1/Option4/txt4.text = options4_1[random_number]
	$AnimationPlayer.play("new_animation")

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func set_buttons():
	if stage == 1:
		if option == 1:
			$cc1/Option1/txt1.text = options1_2[random_number]
			$cc1/Option2/txt2.text = options2_2[random_number]
			$cc1/Option3/txt3.text = options3_2[random_number]
			$cc1/Option4/txt4.text = options4_2[random_number]
		elif option == 2:
			$cc1/Option1/txt1.text = options1_3[random_number]
			$cc1/Option2/txt2.text = options2_3[random_number]
			$cc1/Option3/txt3.text = options3_3[random_number]
			$cc1/Option4/txt4.text = options4_3[random_number]
		elif option == 3:
			$cc1/Option1/txt1.text = options1_4[random_number]
			$cc1/Option2/txt2.text = options2_4[random_number]
			$cc1/Option3/txt3.text = options3_4[random_number]
			$cc1/Option4/txt4.text = options4_4[random_number]
		elif option == 4:
			$cc1/Option1/txt1.text = options1_5[random_number]
			$cc1/Option2/txt2.text = options2_5[random_number]
			$cc1/Option3/txt3.text = options3_5[random_number]
			$cc1/Option4/txt4.text = options4_5[random_number]					
	
func _on_option_1_pressed():
	stage += 1
	if stage == 1:
		$Situation.text = answer1_1[random_number]
		option = 1
	elif stage == 2:
		$cc1.hide()
		$cc2.show()
		if option == 1:
			$Situation.text = answer1_2[random_number]
			$cc2/Right.modulate = Color.RED
			$cc2/Left.modulate = Color.RED
		elif  option == 2:
			$Situation.text = answer2_2[random_number]
			$cc2/Right.modulate = Color.RED
			$cc2/Left.modulate = Color.RED
		elif  option == 3:
			$Situation.text = answer3_2[random_number]
			$cc2/Right.modulate = Color.RED
			$cc2/Left.modulate = Color.RED
		elif  option == 4:
			$Situation.text = answer4_2[random_number]
			$cc2/Right.modulate = Color.RED
			$cc2/Left.modulate = Color.RED						
	set_buttons()
	

func _on_option_2_pressed():
	stage += 1
	if stage == 1:
		$Situation.text = answer2_1[random_number]
		option = 2
	elif stage == 2:
		$cc1.hide()
		$cc2.show()
		if option == 1:
			$Situation.text = answer1_3[random_number]
			$cc2/Right.modulate = Color.RED
			$cc2/Left.modulate = Color.RED
		elif  option == 2:
			$Situation.text = answer2_3[random_number]
			$cc2/Right.modulate = Color.YELLOW
			$cc2/Left.modulate = Color.YELLOW
		elif  option == 3:
			$Situation.text = answer3_3[random_number]
			$cc2/Right.modulate = Color.GREEN
			$cc2/Left.modulate = Color.GREEN
		elif  option == 4:
			$Situation.text = answer4_3[random_number]
			$cc2/Right.modulate = Color.AQUA
			$cc2/Left.modulate = Color.AQUA	
	set_buttons()
		
	
func _on_option_3_pressed():
	stage += 1	
	if stage == 1:
		$Situation.text = answer3_1[random_number]
		option = 3
	elif stage == 2:
		$cc1.hide()
		$cc2.show()
		if option == 1:
			$Situation.text = answer1_4[random_number]
			$cc2/Right.modulate = Color.RED
			$cc2/Left.modulate = Color.RED
		elif  option == 2:
			$Situation.text = answer2_4[random_number]
			$cc2/Right.modulate = Color.YELLOW
			$cc2/Left.modulate = Color.YELLOW
		elif  option == 3:
			$Situation.text = answer3_4[random_number]
			$cc2/Right.modulate = Color.GREEN
			$cc2/Left.modulate = Color.GREEN
		elif  option == 4:
			$Situation.text = answer4_4[random_number]	
			$cc2/Right.modulate = Color.AQUA
			$cc2/Left.modulate = Color.AQUA
	set_buttons()
		


func _on_option_4_pressed():
	stage +=1
	if stage == 1:
		$Situation.text = answer4_1[random_number]
		option = 4
	elif stage == 2:
		$cc1.hide()
		$cc2.show()
		if option == 1:
			$Situation.text = answer1_5[random_number]
			$cc2/Right.modulate = Color.RED
			$cc2/Left.modulate = Color.RED
		elif  option == 2:
			$Situation.text = answer2_5[random_number]
			$cc2/Right.modulate = Color.YELLOW
			$cc2/Left.modulate = Color.YELLOW
		elif  option == 3:
			$Situation.text = answer3_5[random_number]
			$cc2/Right.modulate = Color.GREEN
			$cc2/Left.modulate = Color.GREEN
		elif  option == 4:
			$Situation.text = answer4_5[random_number]
			$cc2/Right.modulate = Color.AQUA
			$cc2/Left.modulate = Color.AQUA	
	set_buttons()



func _on_animation_player_animation_finished(anim_name):
	if anim_name == "write":
		get_parent().change_node("chicken")


func _on_right_pressed():
	if $cc2/Right.modulate == Color.RED:
		Global.chicken_state = Global.chicken.BURNT
		Global.has_chicken = true	
	elif $cc2/Right.modulate == Color.YELLOW:
		Global.chicken_state = Global.chicken.RAW
		Global.has_chicken = true
	elif $cc2/Right.modulate == Color.GREEN:
		Global.chicken_state = Global.chicken.PERFECT
		Global.has_chicken = true
	elif $cc2/Right.modulate == Color.AQUA:
		Global.chicken_state = Global.chicken.ALIEN
		Global.has_chicken = true			
	$cc2.hide()
	$Situation.hide()
	$Label.text = "El pollo ya esta listo"
	$AnimationPlayer.play("write")


func _on_left_pressed():
	if $cc2/Left.modulate == Color.RED:
		Global.chicken_state = Global.chicken.BURNT
		Global.has_chicken = true
	elif $cc2/Left.modulate == Color.YELLOW:
		Global.chicken_state = Global.chicken.RAW
		Global.has_chicken = true
	elif $cc2/Left.modulate == Color.GREEN:
		Global.chicken_state = Global.chicken.PERFECT
		Global.has_chicken = true
	elif $cc2/Left.modulate == Color.AQUA:
		Global.chicken_state = Global.chicken.ALIEN
		Global.has_chicken = true			
	$cc2.hide()
	$Situation.hide()
	$Label.text = "El pollo ya esta listo"
	$AnimationPlayer.play("write")
