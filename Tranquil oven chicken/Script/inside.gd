extends Control

@onready var me : Sprite2D = $Me
@onready var txt : Label = $Textbox/Label

var audios = [load("res://Audio/mumble1.wav"), load("res://Audio/mumble2.wav"), load("res://Audio/mumble3.wav"), load("res://Audio/mumble4.wav")]

var me_back = load("res://Assets/Me_back.png")
var me_front = load("res://Assets/Me_front.png")

var next_text = false
var mouse_in = false
var text_index = 0

var text = ["Hmm... un cliente", "¡Hola! Bienvenido a mi restaurant, ponete comodo.", "Me resultas algo familiar...", "Bue, no importa. ¿Quéres algo de comer?", "Buenisimo, ahi marcho un pollo al horno.", "Toma, mientras divertite con esto."]
var end_texts = ["Bueno, esto si que es un buen pollo. Seguiste tus valores, y el pollo te recompenso. Buen provecho", "Hmm, el pollo se quemo. No es mi culpa, lo trataste mal, es lo que pasa cuando uno es malvado. Toma, quizas podes rescatar algo", "Uy, no se cocino. Tenia tanta pinta este pollo, pero te quedaste en el molde. Aún estas a tiempo", "Ehhh, nunca vi algo parecido. Supongo que la vida es un misterio y aveces hay volantazos raros, ni idea"]

# Called when the node enters the scene tree for the first time.
func _ready():
	$AnimationPlayer.play("slide")
	if Global.has_chicken == false:
		txt.text = text[0]
	else:
		me.texture = me_front
		$Chicken_prop.show()
		
		if Global.chicken_state == Global.chicken.PERFECT:
			txt.text = end_texts[0]
		elif Global.chicken_state == Global.chicken.BURNT:
			txt.text = end_texts[1]	
		elif Global.chicken_state == Global.chicken.RAW:
			txt.text = end_texts[2]
		elif Global.chicken_state == Global.chicken.ALIEN:
			txt.text = end_texts[3]					
	


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func _input(event):
	if event.is_action_pressed("ui_accept") && next_text == true:
		if Global.has_chicken == false:
			next_text = false
			text_index += 1
			$AnimationPlayer.play("write")			
			if text_index < 6:
				txt.text = text[text_index]
				$Me/AudioStreamPlayer2D.stream = audios[randi() % 4]
				$Me/AudioStreamPlayer2D.play()
			else:
				$Textbox.hide()
				$Comecocos.show()
				me.texture = me_back	
		
			if text_index == 1:
				me.texture = me_front
		else:
			$Textbox.hide()	
	
			
	if event.is_action_pressed("Click") && mouse_in == true:
		get_parent().change_node("comecocos")		
		


func _on_animation_player_animation_finished(anim_name):
	if anim_name == "slide":
		$AnimationPlayer.play("write")
		$Me/AudioStreamPlayer2D.play()
	
	if anim_name == "write":
		next_text = true	


func _on_comecocos_mouse_entered():
	$Comecocos/Sprite2D.modulate = Color.YELLOW
	mouse_in = true


func _on_comecocos_mouse_exited():
	$Comecocos/Sprite2D.modulate = Color.WHITE
	mouse_in = false


func _on_audio_stream_player_2d_finished():
	$AudioStreamPlayer2D.play()
