extends Node

var phrase = preload("res://Nodes/phrase.tscn")
var mountains = preload("res://Nodes/mountains.tscn")
var house = preload("res://Nodes/house.tscn")
var bell = preload("res://Nodes/bell.tscn")
var inside = preload("res://Nodes/inside.tscn")
var comecocos = preload("res://Nodes/comecocos.tscn")
var chicken = preload("res://Nodes/chicken.tscn")
var end = preload("res://Nodes/end.tscn")

@onready var timer : Timer = $Timer

var instance

# Called when the node enters the scene tree for the first time.
func _ready():
	instance = phrase.instantiate()
	add_child(instance)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
	
	
func change_node(node_name):
	if node_name == "mountains":
		remove_child(instance)
		instance = mountains.instantiate()
		add_child(instance)
		timer.start()
	elif node_name == "house":
		remove_child(instance)
		instance = house.instantiate()
		add_child(instance)
	elif node_name == "bell":
		remove_child(instance)
		instance = bell.instantiate()
		add_child(instance)			
	elif node_name == "inside":
		remove_child(instance)
		instance = inside.instantiate()
		add_child(instance)
	elif node_name == "comecocos":
		remove_child(instance)
		instance = comecocos.instantiate()
		add_child(instance)	
	elif node_name == "chicken":
		remove_child(instance)
		instance = chicken.instantiate()
		add_child(instance)
	elif node_name == "end":
		remove_child(instance)
		instance = end.instantiate()
		add_child(instance)							


func _on_timer_timeout():
	change_node("house")
	timer.stop()
