extends Control

var perfect = load("res://Assets/Perfect_Chicken.png")
var burnt = load("res://Assets/Burnt_chicken.png")
var raw = load("res://Assets/Raw_chicken.png")
var alien = load("res://Assets/Space_chicken.png")



# Called when the node enters the scene tree for the first time.
func _ready():
	if Global.chicken_state == Global.chicken.PERFECT:
		$TextureRect.texture = perfect
	elif Global.chicken_state == Global.chicken.BURNT:
		$TextureRect.texture = burnt
	elif Global.chicken_state == Global.chicken.RAW:
		$TextureRect.texture = raw
	elif Global.chicken_state == Global.chicken.ALIEN:
		$TextureRect.texture = alien
					
	$AnimationPlayer.play("bounce_in")

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func _on_animation_player_animation_finished(anim_name):
	pass


func _on_audio_stream_player_2d_finished():
	get_parent().change_node("inside")
